# PlayStation Buttons add-on for Dr Robotnik's Ring Racers

This add-on replaces the SEGA Saturn-based button images used in DRRR's menus
with ones based on the Sony PlayStation consoles. The layout is based on the
default controller layout, meaning the buttons correspond like this:

| Saturn Button | Action        | Default Config (Keyboard / Xbox button) | PS button |
|:-------------:|:--------------|:--------------------------:|:----------:|
| A             | Accel / Confirm | `A` / A button           | Cross      |
| B             | Look back       | `LSHIFT` / B button      | Circle     |
| C             | Spin Dash       | `Q` / Y button           | Triangle   |
| X             | Brake / Go back | `D` / X button           | Square     |
| Y             | Respawn         | `V` / L bumper           | L1         |
| Z             | Action          | `Z` / R bumper           | R1         |
| Previous      | Use Item        | `SPACE` / L trigger      | L2         |
| Next          | Drift           | `S` / R trigger          | R2         |

In addition, the Start button was modified to look like the triangle-shaped
Start button of the PS1-PS3 era controllers.

## Installation

Clone this repo into a folder and archive it as a ZIP file (alternatively, just
download the ZIP file of this repo), rename it to have a `.pk3` extension, copy
it to the DRRR add-ons folder and load it in the Extras menu.

Alternatively, check if the "Releases" page of this GitLab repo has any archives
already there.

## Copyright Disclaimer

The images used for the buttons are all edits of the images in the original Ring
Racers PK3 file. Credit to Kart Krew for the original images (as well as the
rest of the game).

The button images are trademarks of Sony.
